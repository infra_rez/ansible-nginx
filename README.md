Ansible Role - nginx
===
An Ansible role to install and configure a base nginx webserver. Also changes firewall rules to allow traffic

# Requirements

- ansible.posix

# Variables
- `webserver` - true or false
- `reverse_proxy` - true or flase
- `server_name` - hostname of the server

# Todo
- make proxy portion. might make a seperate role for proxy.